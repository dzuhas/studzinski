import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  isDisplay = false;
  toggleDisplay(){
    this.isDisplay=!this.isDisplay
  }

  countries = [
    {
      id:'Poland'
    },
    {
      id:"Italy"
    },
    {
      id:"Espana"
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
