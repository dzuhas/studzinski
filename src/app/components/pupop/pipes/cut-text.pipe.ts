import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutText'
})
export class CutTextPipe implements PipeTransform {

  transform(value:  any){
    if (value.length>50){
      return value.substr(0,50)+"...";
    }
    return value;
  }

}
