import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PupopComponent } from './pupop.component';

describe('PupopComponent', () => {
  let component: PupopComponent;
  let fixture: ComponentFixture<PupopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PupopComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PupopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
